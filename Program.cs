﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tasks
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        static int isPrimeSumDivisible(int n) { // sum of all prime numbers less than input must be divisible by input integer

            if (n < 3) // assuming that minimum input will be 3 and anything below 3 will not be considered
                return 0;
            else
            {
                int div_Number = 0;
                for (int i = 2; i < n; i++)
                {
                    if(isPrime(i))
                    {
                        div_Number = div_Number + i;
                    }
                }
                if (div_Number % n == 0)
                    return 1;
                else
                    return 0;
            }
        }

        static bool isPrime(int p)
        {
            int x = p/2;
            for (int i = 2; i <= x; i++)
            {
                if (p % i == 0)
                    return false;
            }
            return true;
        }

        static int isOneTwoOnePatternArray(int[] a)  // number of 1's in the begining and end of an array must be same and in the middle only 2's i.e. 1,2,1 or 1,1,1,2,2,1,1,1
        {
            if (a.Length < 3) // assuming that smallest array will be {1,2,1}
                return 0;
            else if (a[0] != 1) // if first element is not 1 then exit
                return 0;
            else
            {
                int i = 0, count = 0; // i is the number of 1's from start and count is the number of 1's from the end
                for (i = 0; i < a.Length; i++)
                {
                    if (a[i] != 1)
                    {
                        for (int j = (a.Length - 1); j > 0; j--)
                        {
                            if (a[j] == 1)
                                count++;
                            else
                                break;
                        }
                        break;
                    }
                }
                if (i == count)
                {
                    int middle = a.Length - (i * 2);
                    for (int k = 0; k < middle; k++)
                    {
                        if (a[i] != 2)
                            return 0;
                        else
                            i++;
                    }
                    return 1;
                }
                else
                    return 0;
            }
        }
        static int sameNumberOfFactors(int n1, int n2) // both input numbers must have same number of factors
        {
            int factn1_Count = 0, factn2_Count = 0;
            if (n1 == 0 || n2 == 0)
                return 0;
            else if (n1 == n2)
                return 1;
            else if (n1 < 0 || n2 < 0)
                return -1;
            else
            {
                for(int i = 1; i <= n1; i++)
                {
                    if (n1 % i == 0)
                        factn1_Count++;
                }
                for (int j = 1; j <= n2; j++)
                {
                    if (n2 % j == 0)
                        factn2_Count++;
                }
                if (factn1_Count == factn2_Count)
                    return 1;
                else
                    return 0;
            }
        }

        static int[] foo(int [] a) // clone the argument passed by reference and not to sort original input array
        {
            int[] temp_arr = (int[])a.Clone();
            int temp = 0;
            for(int i = 0; i < temp_arr.Length; i++)
            {
                for (int j = i; j < temp_arr.Length; j++)
                {
                    if(temp_arr[i] > temp_arr[j])
                    {
                        temp = temp_arr[i];
                        temp_arr[i] = temp_arr[j];
                        temp_arr[j] = temp;
                    }
                }
            }
            return temp_arr;
        }
        // sample task 2
        static int EvenOddDiff(int[] a)
        {
            int x = 0, y = 0;
            if (a.Length == 0)
                return 0;
            else if (a.Length == 1)
                return 1;
            else
            {
                for(int i = 0; i < a.Length; i++)
                {
                    if (a[i] % 2 == 0)
                        y = y + a[i];
                    else
                        x = x + a[i];
                }
                return x - y;
            }
        }

        // sample task 1
        static int Centered(int[] a)
        {
            if (a.Length > 0)
            {
                if (a.Length == 1)
                    return 1;
                else if (a.Length % 2 == 0)
                    return 0;
                else
                {
                    bool isCentered = true;
                    int cenIdx = a.Length/2;
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (i != cenIdx && a[i] <= a[cenIdx])
                            isCentered = false;
                    }
                    if (isCentered)
                        return 1;
                    else
                        return 0;
                }
            }
            return 0;
        }
    }
}
